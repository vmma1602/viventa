<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Viventa</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">  
        
           
        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        <script type="text/html" src="{{asset("assets/js/funcionesViventa.js")}}" defer></script>
     <link rel="stylesheet" href="{{asset("assets/css/estilosViventa.css")}}">
       
        <script src="https://kit.fontawesome.com/f04b010270.js" crossorigin="anonymous"></script>
     
       
    </head>
    <body class="antialiased fondoHome" >
     
        <nav class="navbar navbar-light fixed-top" id="navbarHome">
            <div class="container-fluid" >
              <a class="navbar-brand" id="NavbarHomeBrand" href="{{url('/')}}"> <span class="recomendacion">Viventa</span></a>
              <form class="d-flex " id="NavbarFormHome">
                <input class="form-control me-4" type="search" placeholder="Encuentra tu lugar" aria-label="Buscar">
                <button type="submit" class="btn btn-outline-light text-nowrap" style="margin-right:20px"> <span><i class="fas fa-search"></i></span> Buscar</button>     
                <a id="linkLogin" href="#" data-bs-toggle="modal" data-bs-target="#modalLogin"><span><i class="fas fa-user" id="userIcon" ></i></span></a>
              </form>
            </div>
          </nav>

          @yield('Contenido')
          @yield('Footer')



       <!-- Esto se queda oculto, no afecta a lo demás -->
          <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="tituloVentana" aria-hidden="true"> 
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">            
                  <h5 id="tituloVentana centrar" style="color: blue;"> Viventa </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">               
                    </button>
                </div>       
               
               <div class="modal-body">
                  <div class="container">
                    <div class="row centrar">                
                        <div class="col-sm-12 col-md-12 col-lg-12">
                          <form method="POST">
                            @csrf 
                            <div class="mb-3">
                                 <input name="email" type="email" class="form-control" id="emailUser" aria-describedby="emailHelp"
                                 placeholder="Usuario">
                                
                               </div>
                               <div class="mb-3">              
                                 <input  name="password" type="password" class="form-control" id="passUser" placeholder="Contraseña">
                                 <div id="passHelp" class="form-text"> <span><i class="fas fa-lock"></i></span> No compartiremos tu información con nadie mas</div>
                               </div>  
                                                                      
                        </div>
                    </div>   
                    
                          <div class="row">                   
                            <button type="submit" class="btn btn-primary ">Ingresar</button>
                          </div>     
                        </form>         
                  </div>
                </div>
                <div class="modal-footer">
                      <div class="container">
                         
                          <div class="row centrar">   
                            <span>Siguenos en nuestras redes sociales <br> <br></span>
                          
                          </div>
                          <div class="row centrar">   
                          <div class="col-sm-4 col-lg-4"><a href="#" style="text-decoration: none;"><span class="socialMedia" style="color: crimson;"><i class="fab fa-google socialMedia"></i></span></a></div>
                          <div class="col-sm-4 col-lg-4"><a href="#" style="text-decoration: none;"><span class="socialMedia" style="color:blue"><i class="fab fa-facebook"></i></a></span></div>
                          <div class="col-sm-4 col-lg-4"><a href="#" style="text-decoration: none;"><span class="socialMedia" style="color: dodgerblue;"><i class="fab fa-twitter"></i></a></span></div>
                          </div>
        
                          <div class="row centrar">
                            <span> <br>¿Aun no eres miembro? <a href="{{url('/registro')}}">Registrate</a> <br></span>
                          </div>
                            
                      </div>
                </div>                           
              </div>
            </div>
          </div>

    </body>
</html>
    
        