@extends('navfoot')
@section('Contenido')

        <div class="showcase p-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-2"></div>
                    <div class="col-sm-12 col-lg-8">
                        <form class="bg-light p-5 text-dark rounded">
                            <h2 class="centrar">Registro</h2>
                            <div class="mb-2 form-group">
                                <label class="mb-2" for="nombre">Nombre(s)</label>
                                <input class="form-control" type="text" id="nombre" >
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="appaterno">Apellido Paterno</label>
                                <input class="form-control" type="text" id="appaterno" >
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="apmaterno">Apellido Materno</label>
                                <input class="form-control" type="text" id="apmaterno" >
                            </div>
                            <div class="form-group mb-3">
                                <label class="mb-2" for="tipocuenta">Tipo de cuenta</label>
                                <select class="form-select " aria-label="" id="tipocuenta">
                                    <option>Seleciona una opción</option>
                                    <option value="1">Publicar</option>
                                    <option value="2">Buscar</option>
                                </select>
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="direccion">Direccion</label>
                                <input class="form-control" type="text" id="direccion" >
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="correo">Correo Electronico</label>
                                <input class="form-control" type="text" id="correo">
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="contrasenia">Contraseña</label>
                                <input class="form-control" type="password" id="contrasenia" placeholder="Escriba su contraseña">
                            </div>
                            <div class="form-group mb-2">
                                <label class="mb-2" for="password">Confirmar contraseña</label>
                                <input class="form-control" type="password" id="password" placeholder="Ingrese nuevamente su contraseña" >
                            </div>
                            <br>
                            <center><button class="btn btn-primary btn-block" type="submit">Guardar registro</button></center>
                        </form>
                    </div>
                    <div class="col-sm-12 col-lg-2"></div>
                </div>
            </div>
        </div>



@endsection