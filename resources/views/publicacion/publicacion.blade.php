<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publicacion Anuncio
    </title>

     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

     <!-- Styles -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">  
    
        
        
     <!-- Scripts -->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
     <script src="{{asset("assets/js/funcionesViventa.js")}}" defer></script>
     <link rel="stylesheet" href="{{asset("assets/css/estilosViventa.css")}}">
     <script src="https://kit.fontawesome.com/f04b010270.js" crossorigin="anonymous"></script>


</head>
<body>
  <nav class="navbar navbar-light " id="navbarHome">
    <div class="container-fluid" >
      <a class="navbar-brand" id="NavbarHomeBrand" href="Index.html"> <span class="recomendacion">Viventa</span></a>
      <form class="d-flex " id="NavbarFormHome">
        <input class="form-control me-4" type="search" placeholder="Encuentra tu lugar" aria-label="Buscar">
        <button type="submit" class="btn btn-outline-light text-nowrap" style="margin-right:20px"> <span><i class="fas fa-search"></i></span> Buscar</button>
        <a href="login.html"> <span><i class="fas fa-user" id="userIcon"></i>  </span></a>
      </form>
    </div>
  </nav>

      <br>
      <h1 style="color:#191970">Crea tu propio anuncio!</h1>
<br>

      <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-1">
            </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
      <h4 style="color:#191970">Tus imagenes</h4>
      
      <h5 style="color:#191970">1/3</h5>
      <div class="logo">
        <img src="{{asset("assets/img/sala.jpg")}}">
        </div>
       <span style="color: #90EE90">Independizar</span>
       <br>
       <span style="color: purple">Departamento pequeño</span>
       <p>Agrega etiquetas</p>
        </div>
        
        <div class="col-sm-12 col-md-4 col-lg-3">
       <h3 style="color: #90EE90">Datos del inmueble</h3>
       <h2 style="color:#191970">VENTA</h2>
       <span>Costo</span> <span style="color: red">$ 1 800 000</span>
       <p>Descripcion</p>

       <p> > Tres habitaciones</p>
       <p> > Dos baños</p>
       <div class="dropdown show">
        <a class="btn  dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tipo de inmueble
        </a>
      
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="#">Casa</a>
          <a class="dropdown-item" href="#">Departamento</a>
          <a class="dropdown-item" href="#">Cabaña</a>
        </div>
      </div>
       </div>
       <div class="col-sm-12 col-md-4 col-lg-5">
           <p style="color:#191970">Selecciona la ubicación</p>
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2847.017623176479!2d-101.93451869977737!3d21.360494492315084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842bd24b39db7177%3A0x7456bacea2fc48e8!2sConstituyentes%20Norte%20276%2C%20El%20Calvario%2C%2047420%20Lagos%20de%20Moreno%2C%20Jal.!5e0!3m2!1ses-419!2smx!4v1621310653145!5m2!1ses-419!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

           <button type="button" class="btn btn-danger">Cancelar</button> <button type="button" class="btn btn-primary">Publicar</button>
           </div>
     
     
        </div>
      </div>
    
</body>
</html>