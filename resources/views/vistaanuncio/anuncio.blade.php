<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inmueble</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="{{asset("assets/js/funcionesViventa.js")}}" defer></script>
     <link rel="stylesheet" href="{{asset("assets/css/estilosViventa.css")}}">

</head>
<body>
    <nav class="navbar navbar-light navbar-fixed-top" id="navbarHome">
        <div class="container-fluid" >
            <a class="navbar-brand" id="NavbarHomeBrand" href="#">Viventa</a>
            <form class="d-flex" id="NavbarFormHome">
                <input class="form-control me-4" type="search" placeholder="Encuentra tu lugar" aria-label="Buscar">
                <button type="submit" class="btn btn-outline-light text-nowrap" >Buscar</button>
            </form>
        </div>
    </nav>

    <div class="container">
        <br><br>
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/carrusel1.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="img/carrusel2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="img/carrusel3.jpg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <br><h3 class="text-success">$4,500.00</h3>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="align-rigth ">
                    <div class="row">
                        <div class="col-lg-10 col-sm-12" >
                            <h4>Casa residencial Lagos de Moreno</h4><br>
                        </div>
                        <div class="col-lg-2 col-sm-12 text-danger">

                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-suit-heart" viewBox="0 0 16 16">
                                <path d="m8 6.236-.894-1.789c-.222-.443-.607-1.08-1.152-1.595C5.418 2.345 4.776 2 4 2 2.324 2 1 3.326 1 4.92c0 1.211.554 2.066 1.868 3.37.337.334.721.695 1.146 1.093C5.122 10.423 6.5 11.717 8 13.447c1.5-1.73 2.878-3.024 3.986-4.064.425-.398.81-.76 1.146-1.093C14.446 6.986 15 6.131 15 4.92 15 3.326 13.676 2 12 2c-.777 0-1.418.345-1.954.852-.545.515-.93 1.152-1.152 1.595L8 6.236zm.392 8.292a.513.513 0 0 1-.784 0c-1.601-1.902-3.05-3.262-4.243-4.381C1.3 8.208 0 6.989 0 4.92 0 2.755 1.79 1 4 1c1.6 0 2.719 1.05 3.404 2.008.26.365.458.716.596.992a7.55 7.55 0 0 1 .596-.992C9.281 2.049 10.4 1 12 1c2.21 0 4 1.755 4 3.92 0 2.069-1.3 3.288-3.365 5.227-1.193 1.12-2.642 2.48-4.243 4.38z"/>
                            </svg>
                        </div>
                    </div>

                </div>

                <div class="card" >
                    <div class="card-body border border-2">
                        <h5 class="card-title">Descripción</h5>
                        <p class="card-text">Ubicada en un fraccionamiento con un impacto grande en cuanto a seguridad, con vista tranquila hacia el centro de la ciudad</p>
                        <h6>Incluye</h6>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">3 recamaras con baño</li>
                            <li class="list-group-item">Sala amplia</li>
                            <li class="list-group-item">Cocina y comedor</li>
                            <li class="list-group-item">Cochera para dos autos</li>
                        </ul>

                    </div>
                </div>
                <br>
                <div class="container form-control">
                    <div class="mb-3">
                        <label for="mensaje" class="form-label">Contactar al anunciante</label>
                        <textarea class="form-control" id="mensaje" rows="2"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-lg-9 col-sm-9"></div>
                        <div class="col-lg-3 col-sm-3">
                            <button class="btn btn-primary btn-block" type="submit">Enviar</button>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="card" >
                    <img src="img/carrusel2.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Departamento Lagos de Moreno</h5>
                        <p class="card-text">Departamento en venta, ubicado en el centro historico de Lagos de Moreno
                        </p>
                        <a href="#" class="btn btn-primary">Ver anuncio</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="card" >
                    <img src="img/carrusel1.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Pension en Leon Gto.</h5>
                        <p class="card-text">Venta de inmueble ubicada en León Guanajuato</p>
                        <a href="#" class="btn btn-primary">Ver anuncio</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="card" >
                    <img src="img/carrusel3.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Renta de cuartos</h5>
                        <p class="card-text">Disfruta de la comodidad de hospedarte el Lagos de Moreno</p>
                        <a href="#" class="btn btn-primary">Ver anuncio</a>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>


</body>
</html>