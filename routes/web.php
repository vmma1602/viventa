<?php

use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/registro', ['as'=>'registro', function(){
    return view ('/Registros.registro');
}]);
Route::get('/anuncio', ['as'=>'anuncio', function(){
    return view ('/vistaanuncio.anuncio');
}]);
Route::get('/publicacion', ['as'=>'publicacion', function(){
    return view ('/publicacion.publicacion');
}]);

Route::get('/', ['as'=>'principal', function(){
    return view ('/principal');
}]);
Route::post('/', function(){
    $credentials = request()->only('email','password');
   
    if (Auth::attempt($credentials, false)){
    return "Exitoso";
    }
    else{
        return "No exitoso bro";
    }
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

function validar(){
    
}